import 'react-native-gesture-handler';
import React , {Component, useState, useEffect} from "react";
import { View,Text,Button,StyleSheet,
TouchableOpacity,Image,ActivityIndicator,StatusBar
} from "react-native";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import MainMenu from "./MainMenu";
import Login from "./Login";
import Splash from "./SplashScreen";


function HomeScreen({navigation}) {                                   //home screen functional component
  return (
     <View style = {styles.container}>
      <StatusBar                                                          //added status bar color
      barStyle = "dark-content"
      backgroundColor = "#e67e22"
      />
      <Image                                                             //added image
           style = {styles.img2}
           source={require("./img/playstore-icon.png")}
          />
      <TouchableOpacity style ={styles.screen}                           //added button
      onPress={() => navigation.navigate('Login')}>
          <Text style={styles.loginText}>LOGIN PAGE</Text>
      </TouchableOpacity>
      <TouchableOpacity style ={styles.screen}
      onPress={() => navigation.goBack()}>
          <Text style={styles.loginText}>BACK</Text>
      </TouchableOpacity>
     </View>
  );
}

const Stack = createStackNavigator();                                            

function App() {                                                 //creating function for making routes for navigation
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Splash"
      screenOptions={{
          headerStyle: {
              backgroundColor: '#e67e22',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
          fontWeight: 'bold',
        },
        }}
      >
        <Stack.Screen
              name="Home" component={HomeScreen} 
               options={{
            title: 'Home Screen',
          }}
       />
        <Stack.Screen name="Splash" component={Splash}
         options={{title:"",
         headerStyle: {
              backgroundColor: '#003f5c',
            },
            headerTintColor: '#fff',
          }}
         />
        <Stack.Screen name="Login" component={Login} 
          options={{
            title: 'Login Page',  
          }}
        />
        <Stack.Screen name="Main Menu" component={MainMenu} 
          options={{
            title: 'Main Menu', 
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;

const styles = StyleSheet.create({                                  //stylesheet for components
    view:{
       flex: 1, 
       alignItems: 'center', 
       justifyContent: 'center',
    },
    container:{
       backgroundColor:'#003f5c',
       flex:1,
       alignItems:'center',
       justifyContent:"center",
    },
    img:{
      width:400,
      height:300,

    },
    img2:{
      width:300,
      height:300,
    },
    title:{
       fontWeight:'bold',
       fontSize:18, 
    },
    fast:{ 
        margin:20,
        width: 100,
        height: 100,
    },
    loginText:{
    color:"white",
    fontSize:20,
  },
    screen:{
    width:"80%",
    backgroundColor:"#e67e22",
    borderRadius:25,
    height:50,
    alignItems:"center",
    justifyContent:"center",
    marginTop:40,
    marginBottom:10
   },
})
