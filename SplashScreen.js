import React , {Component, useState, useEffect} from "react";
import { View, Text, TextInput, StyleSheet, TouchableOpacity, StatusBar,Image } from "react-native";
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';


export default class Splash extends Component{                      //it is the Splash screen class component

  constructor(props){                                                   //passing props
    super(props);
    setTimeout(() => {
      this.props.navigation.navigate("Home");                       //after 3 sec it will atomatically navigate to home screen
      // console.log('====================================');
      // console.log("Loading after 3 seconds");
      // console.log('====================================');
    }, 4000);
  }

  render(){
  return (
    <View style={styles.view}>
       <StatusBar                                                       
           barStyle = "dark-content"
           backgroundColor = "#003f5c"
       />
       <Image 
          style = {styles.img}
          //  source={require("./img/playstore-icon.png")}
           source={require("./img/giphy.gif")}
          />
          <Text style={styles.Text} >MY APP</Text>
     {/* <TouchableOpacity style ={styles.screen}                                       
        onPress={() =>  this.props.navigation.navigate("Start")}>                       
        <Text style={styles.loginText}>NEXT</Text>
    </TouchableOpacity>                       */}
    </View>
  );
}
}

const styles= StyleSheet.create({                    //stylesheet for components
    view:{
      backgroundColor:'#003f5c',
       flex: 1, 
       alignItems: 'center', 
       justifyContent: 'center',
    },
    container:{
       backgroundColor:'#003f5c',
       flex:1,
       alignItems:'center',
       justifyContent:"center",
    },
    img:{
      width:395,
      height:350,
    },
    img2:{
      width:300,
      height:300,
    },
    title:{
       fontWeight:'bold',
       fontSize:18, 
    },
    fast:{ 
        margin:20,
        width: 100,
        height: 100,
    },
    loginText:{
    color:"white",
    fontSize:20,
  },
  Text:{
    color:"black",
    fontWeight:'bold',
    fontSize:60,
  },
    screen:{
    width:"80%",
    backgroundColor:"#e67e22",
    borderRadius:25,
    height:50,
    alignItems:"center",
    justifyContent:"center",
    marginTop:40,
    marginBottom:10
   },
})