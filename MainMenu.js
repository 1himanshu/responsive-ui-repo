import * as React from "react";
import { View,Text,Button,TextInput,StyleSheet,TouchableOpacity } 
from "react-native";
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

function MainMenu ({navigation}) {                                                //it is last screen functional component
        return(
            <View style = {styles.container}>
                <Text style = {styles.txt}>
                WELCOME! To Main Menu</Text>
                <TouchableOpacity style ={styles.back}                            //navigating to homescreen
                 onPress={() => navigation.navigate('Home')}>                    
                <Text style={styles.loginText}>HOME</Text>
                </TouchableOpacity>       
                </View>
        );
    }

export default MainMenu ;

const styles= StyleSheet.create({
    container:{
     flex:1,
     backgroundColor:"#003f5c",
     alignItems:'center',
     justifyContent:"center",
   },
   txt:{
     color:"orange",
     fontSize:50,
     fontWeight:'bold',
      margin:20,
   },
   loginText:{
    color:"white",
    fontSize:20,
  },
   back:{
    width:"80%",
    backgroundColor:"#e67e22",
    borderRadius:25,
    height:50,
    alignItems:"center",
    justifyContent:"center",
    marginTop:40,
    marginBottom:10
  },
})