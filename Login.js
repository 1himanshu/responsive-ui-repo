import React , {Component, useState} from "react";
import { 
  View,Text,Image,Button,TextInput,
  StyleSheet,TouchableOpacity,} 
from "react-native";
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

function Login ({navigation}) {                                                //login component
  const [email, setEmail] = useState('');                                      //using hooks to store email and password values
  const [password, setPassword] = useState('');

    return(
         <View style ={styles.container}>
           <Image 
          style = {styles.img}
           source={require("./img/l2.png")}
          />
           {/* <Text style ={styles.txt}>LOGIN SCREEN</Text> */}
           <TextInput style ={[styles.inputView,styles.inputText]}             //username input component
           placeholder="Username ..."
           placeholderTextColor="white"
           keyboardType="email-address"
           returnKeyType="next"
           onChangeText={email => setEmail(email)}
           defaultValue={email}
      />
           
           <TextInput style={[styles.inputView,styles.inputText]}                   //password input component
          placeholder='Password ...'
          placeholderTextColor='white'
          secureTextEntry
          keyboardType="number-pad"
          returnKeyType="go"
          onChangeText={password => setPassword(password)}
          defaultValue={password} 
          maxLength={4}
          />
          <TouchableOpacity>
            <Text style={styles.forgot}>Forgot Password?</Text>
          </TouchableOpacity>
          <TouchableOpacity style ={styles.loginBtn}
          onPress={() => navigation.navigate('Main Menu')}>
          <Text style={styles.loginText}>SIGN IN</Text>
          </TouchableOpacity>
          <TouchableOpacity
           onPress={() => navigation.push('Login')}>
          <Text style={styles.loginText}>SIGN UP</Text>
        </TouchableOpacity>
         </View>
     );
}
export default Login;

const styles = StyleSheet.create({
   container:{
     flex:1,
     backgroundColor:"#003f5c",
     alignItems:'center',
     justifyContent:"center",
   },
   img:{
      width:400,
      height:250,
    },
   txt:{
     color:"orange",
     fontSize:50,
     fontStyle:'italic',
     fontWeight:'bold',
      marginBottom:40,
   },
   inputView:{
      width:"85%",
     backgroundColor:"#465881",
     borderRadius:25,
     height:50,
     marginBottom:20,
     justifyContent:"center",
     padding:20
  },
  inputText:{
    height:60,
    color:"white"
  },
  forgot:{
    color:"white",
    fontSize:15
  },
  loginText:{
    color:"white",
    fontSize:15
  },
   loginBtn:{
    width:"80%",
    backgroundColor:"#e67e22",
    borderRadius:25,
    height:50,
    alignItems:"center",
    justifyContent:"center",
    marginTop:40,
    marginBottom:10
  },
});